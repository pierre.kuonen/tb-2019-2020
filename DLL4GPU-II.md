---
version: 1
titre:  DLL4GPU-II / Amélioration des performances de l'outil DLL
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Jean Hennbert
  - Beat Wolf
  - Baptiste Wicht (externe) 
mots-clé: [Machine Learning, GPU, vectorisation, C++]
langue: [F]
proposé par étudiant: Nicolas Realini
confidentialité: non
suite: oui
---

## Contexte
Ces dernières années, l'utilisation du Machine Learning a connu une montée en flèche dans tous les domaines de la science et de la technologie. Ce type d'algorithme aborde le problème d'une manière radicalement différente par rapport aux méthodes traditionnelles. Les techniques de Deep Learning (DL) permettent au système de s'entraîner lui-même en utilisant des données déjà existantes. Plusieurs plateformes et outils existent pour permettre aux programmeurs de développer des applications de Deep Learning. L'une d'entre elles, appelée DLL (Deep Learning Library) a été développée à iCoSys (Institute of Complex Systems) dans le cadre d'une thèse de doctorat. Cette bibliothèque permet des implémentations très rapides de différents types de réseaux de neurones sur des CPUs mais n'est actuellement pas très efficace lors de l'utilisation de GPUs. Un projet PS6 a commencé à s'attaquer à ce problème mais DLL est un logiciel C++ très complexe faisant un usage intensif de "template".


## Objectifs

L'objectif de ce projet est de poursuivre les premiers travaux réalisés au cours du PS6 pour accélérer le calcul sur GPGPU et, si le temps le permet de faire également usage des possibilités de vectorisations des processeurs récents.
