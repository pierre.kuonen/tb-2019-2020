---
version: 1
titre: Modélisation de la consomation électrique - II
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Jean Hennbert
  - Beat Wolf
  - Daniel Wagner (ES)
mots-clé: [Machine learning, distribution électrique]
langue: [F]
proposé par étudiant: Buntschu Adrian
confidentialité: oui
suite: oui
---


## Contexte

Electricité de Strasbourg (ES) distribue et commercialise de l’électricité (Mais aussi du gaz et de la chaleur) à 550 000 clients dans le Bas-Rhin, France. Pour l’instant, ES ne produit quasiment pas d’électricité  et l’entreprise se source sur les marchés de l’électricité pour des volumes à long terme (1-3 ans) et à court terme (j+1 à j+30).
Comme 100 % du marché de l’électricité est ouvert à la concurrence, l’entreprise doit être capable de mieux cibler ses campagnes de marketing et prévoir pour chaque client sa demande en énergie afin d’optimiser ses volumes d’achats et ainsi de minimiser ses risques au niveau des volumes achetés sur les marchés.

## Objectifs

Le but ultime du projet consiste à réaliser une classification des clients en deux classes :
- Ceux utilisant l’électricité pour le chauffage (que ce soit du chauffage électrique direct (radiateurs électriques, plancher chauffant) ou une pompe à chaleur)
- Ceux n’utilisant pas l’électricité pour le chauffage

Pour réaliser ce travail, ES met à disposition une base de données de clients (environ 100 000) pour lesquels on dispose:
- du type de chauffage utilisé par le client.
- des relevés de compteur d’électricité du logement sur une période de 12 ans 
- de l'historique des températures extérieures, du rayonnement solaire depuis 1961
- d’autres données qualitatives telles que : surface du logement, date de mise en service... etc 

A partir de ces données, en utilisant des techniques de machine learning, on veut déterminer un modèle de la consommation annuelle d’un logement car, pour les logement neufs, ES ne dispose d’aucun moyen pour estimer la consommation future d’un logement. Ce projet est une continuation d'un projet PS6.

