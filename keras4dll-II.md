---
version: 1
titre:  Keras4DLL-II / Adpation de KERAS pour l'utilisation DLL
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Jean Hennbert
  - Beat Wolf
  - Baptiste Wicht (externe)
mots-clé: [Machine Learning,Keras]
langue: [F]
proposé par étudiant: Iseli Yael
confidentialité: non
suite: oui
---

## Contexte
Ces dernières années, l'utilisation du Machine Learning a connu une montée en flèche dans tous les domaines de la science et de la technologie. Ce type d'algorithme aborde le problème d'une manière radicalement différente par rapport aux méthodes traditionnelles. Les techniques de Deep Learning (DL) permettent au système de s'entraîner lui-même en utilisant des données déjà existantes. Plusieurs plateformes et outils existent pour permettre aux programmeurs de développer des applications de Deep Learning. L'une d'entre elles, appelée DLL (Deep Learning Library) a été développée à iCoSys (Institute of Complex Systems) dans le cadre d'une thèse de doctorat. DLL est toutefois un outil d'assez bas niveau dont l'utilisation nécessite de très bonnes connaissances du langage C++. Aujourd'hui les ingénieurs utilisent des plateformes de plus haut niveau qui abstraient une partie des difficultés techniques de la réalisation d'un application de Deep Learning. Une de ces plateformes ayant un grand succès est la plateforme KERAS (https://keras.io/) écrite en Python. Cette plateforme est, en principe, prévue pour pouvoir utiliser des outils de plus bas niveau comme l'outil TensorFlow (www.tensorflow.org/). 

## Objectifs

L'objectif ultime de ce projet est de permettre à KERA d'utiliser l'outil DLL. Il s'agit d'une tâche particulièrement complexe car DLL est un logiciel complexe écrit en C++ (et non Python) et fait un usage intensif des "template". Un premier projet PS6 a permis de débroussailler le problème et de faire cette liaison pour des problèmes très simples. Il s'agit de continuer ce travail afin de l'étendre à une catégorie de problèmes la plus large possible.
