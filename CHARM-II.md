---
version: 1
titre: CHARM project Hazard perception - II
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Jean Hennbert
  - Beat Wolf 
mots-clé: [Machine Learning, voiture autonome]
langue: [F]
proposé par étudiant: Kevin Buergisser
confidentialité: non
suite: oui
---


## Contexte

Les voitures d'aujourd'hui possède de plus en plus de systèmes d'aide à la conduite et les premiers véhicules autonomes commencent à rouler  en milieu réel. Ceci est possible, entre autres,  grâce aux systèmes avancés d'aide à la conduite (ADAS:  Advanced Driver Assistance Systems). Ces systèmes sont capables de détecter les danger auxquels le véhicule est exposé et, en cas d'urgence, d'avertir le conducteur ou de freiner automatiquement. Ces fonctions sont réalisées grâce à l'utilisation de divers capteurs et équipements tels que des capteurs à ultrasons, des caméras,  des radars ou de la télédétection par laser (lidar: light detection and ranging ) qui se concentrent sur la perception de l'environnement extérieur du véhicule.  


## Objectifs

Le but du présent projet est de réalisé, en collaboration avec le projet CHARM (Context-aware Human Activity Recognition and Monitoring for intelligent vehicles), un prototype de détection des dangers extérieurs aux véhicule. CHARM est un projet auquel participent plusieurs universités de Grande Bretagne et d'Inde. Dans le cadre de ce projet L'HEIA-FR collabore principalement avec l'université anglaise de Edge Hill.



