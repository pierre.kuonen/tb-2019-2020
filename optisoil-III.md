---
version: 1
titre: Optisoil III / Artificial Intelligence applied to the process of excavation for buildings construction
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Jean Hennbert
  - Beat Wolf
  - Stéphane Commend (iTEC)
  - Colette Jost (iTEC)
mots-clé: [Machine Learning, simulation numérique, génie civil]
langue: [F]
proposé par étudiant: Amanda Hayoz
confidentialité: non
suite: oui
---


## Contexte

Dans le domaine du génie civil, il est usuel de faire des fouilles dans le cadre de la construction d’un bâtiment. Pour des raisons de sécurité évidentes, il faut s’assurer que ces fouilles soient stables et bien maintenues par des étais. Actuellement, il existe un programme de simulation numérique (ZSWalls) qui est capable de calculer le comportement de la fouille pour un agencement donné, ainsi qu’un programme entraînant un réseau neuronal permettant de calculer rapidement les meilleures solutions d’après des pré-conditions choisies. Les précédentes contributions ont permis d’avoir un réseau neuronal ayant des résultats prometteurs. Pour l’instant, il n’a été testé qu’avec des résultats synthétiques générés par la simulation numérique. Dès lors, il s’agit de valider le comportement et les prédictions du réseau neuronal avec des tests supplémentaires dont des cas réels et d’analyser le comportement et les prédictions du réseau neuronal actuel pour mesurer sa performance. Ce projet est une suite d'un projet PS6 qui a abouti à la réalisation d'un prototype démontrant l'intérêt et la faisabilité d'une telle approche.


## Objectifs


Le but du projet de bachelor est de réaliser un prototype pré-industriel utilisable par des non-informaticiens. En particulier la notion de "meilleure solution" est complexe à définir et dépendant fortement de l'utilisateur. Une approche de type "système expert" est envisagée pour résoudre cette difficulté. Il faut également améliorer l'interface utilisateur et, dans la mesure du temps disponible, faire une version en ligne de cette application. Ce projet est réalisé en collaboration avec l'institut des Technologies de l'Environnement Construit (iTEC).

